package debug

import java.text.SimpleDateFormat
import java.util.Date

trait DebugColors {

  /** Foreground color for ANSI */
  final val BLACK = "\u001b[30m"
  final val DARK_RED = "\u001b[31m"
  final val DARK_GREEN = "\u001b[32m"
  final val DARK_YELLOW = "\u001b[33m"
  final val DARK_BLUE = "\u001b[34m"
  final val DARK_MAGENTA = "\u001b[35m"
  final val DARK_CYAN = "\u001b[36m"
  final val LIGHT_GRAY = "\u001b[37m"

  final val GRAY = "\u001b[90m"
  final val RED = "\u001b[91m"
  final val GREEN = "\u001b[92m"
  final val YELLOW = "\u001b[93m"
  final val BLUE = "\u001b[94m"
  final val MAGENTA = "\u001b[95m"
  final val CYAN = "\u001b[96m"
  final val WHITE = "\u001b[97m"

  /** Background color for ANSI black */
  final val BLACK_B = "\u001b[40m"
  final val RED_B = "\u001b[41m"
  final val GREEN_B = "\u001b[42m"
  final val YELLOW_B = "\u001b[43m"
  final val BLUE_B = "\u001b[44m"
  final val MAGENTA_B = "\u001b[45m"
  final val CYAN_B = "\u001b[46m"
  final val WHITE_B = "\u001b[47m"

  /** Reset ANSI styles */
  final val RESET = "\u001b[0m"
  /** ANSI bold */
  final val BOLD = "\u001b[1m"
  /** ANSI underlines */
  final val UNDERLINED = "\u001b[4m"
  /** ANSI blink */
  final val BLINK = "\u001b[5m"
  /** ANSI reversed */
  final val REVERSED = "\u001b[7m"
  /** ANSI invisible */
  final val INVISIBLE = "\u001b[8m"

}

object Debug extends DebugColors {
  private var color = GREEN
  private val dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  private val showTimestamps = false

  def setColor(color: String) = this.color = color

  def log(text: Any) =
    println(this.color + (if (showTimestamps) "[" + dFormat.format(new Date()) + "] " else "") + text.toString + RESET)

  def log(text: Any, color: String) =
    println(color + (if (showTimestamps) "[" + dFormat.format(new Date()) + "] " else "") + text.toString + RESET)

  def red(text: Any) = log(text, RED)

  def green(text: Any) = log(text, GREEN)

  def yellow(text: Any) = log(text, YELLOW)

  def blue(text: Any) = log(text, BLUE)

  def magenta(text: Any) = log(text, MAGENTA)

  def cyan(text: Any) = log(text, CYAN)

  def gray(text: Any) = log(text, LIGHT_GRAY)

  def darkRed(text: Any) = log(text, DARK_RED)

  def darkGreen(text: Any) = log(text, DARK_GREEN)

  def darkYellow(text: Any) = log(text, DARK_YELLOW)

  def darkBlue(text: Any) = log(text, DARK_BLUE)

  def darkMagenta(text: Any) = log(text, DARK_MAGENTA)

  def darkCyan(text: Any) = log(text, DARK_CYAN)

  def darkGray(text: Any) = log(text, GRAY)

  def line(color: String = GRAY) = log("-----------------------------------------------------------", color)

  def line: Unit = line(GRAY)

  def doubleLine(color: String = GRAY) = log("===========================================================", color)
}

