import java.io.{BufferedWriter, File, FileWriter}
import scala.concurrent.duration.{Duration, DurationLong}
import scala.language.postfixOps
import scala.util.Random

object CollectionsPerformance extends App {

  private def ranges(base: Int, n: Int): Seq[Range] = for {
    i <- 1 until n
  } yield 1 until math.pow(base, i).toInt

  private def measureAvgExecTime[T](n: Int)(op: => T): Duration = {
    def measureExecTime: Long = {
      val startTime = System.nanoTime()
      op
      val endTime = System.nanoTime()
      endTime - startTime
    }

    val values: Seq[Long] = (1 until n).map(_ => measureExecTime)
    (values.sum / n).nanoseconds
  }

  val iterations: Int = 27
  val base: Int = 2
  val repeats: Int = 10
  lazy val ranges: Seq[Range] = ranges(base, iterations)

  def runThis(): Unit = {
    val file = new File("plot.csv")
    println("file is at: " + file.getAbsolutePath)
    val bw = new BufferedWriter(new FileWriter(file))
    val header = s"No elems,Seq apply,List apply,Vector apply,Seq append,List append,Vector append,Seq prepend,List prepend,Vector prepend\n"
    bw.write(header)
    for {
      range <- ranges
    } {
      val els = range.size
      val seq: Seq[Int] = range
      val list: List[Int] = range.toList
      val vector: Vector[Int] = range.toVector

      def measureAvg = measureAvgExecTime(repeats) _

      val random: Int = Random.nextInt(els)

      val applySeq = measureAvg(seq(random))
      val applyList = measureAvg(list(random))
      val applyVector = measureAvg(vector(random))

      val appendSeq = measureAvg(seq :+ random)
      val appendList = measureAvg(list :+ random)
      val appendVector = measureAvg(vector :+ random)

      val prependSeq = measureAvg(random +: seq)
      val prependList = measureAvg(random +: list)
      val prependVector = measureAvg(random +: vector)

      val plotChunk = s"$els,$applySeq,$applyList,$applyVector,$appendSeq,$appendList,$appendVector,$prependSeq,$prependList,$prependVector\n"
      bw.write(plotChunk)
    }

    bw.close()
  }

  runThis()

}
